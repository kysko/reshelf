﻿using AutoMapper;

using System;
using System.Collections.Generic;
using System.Text;

namespace Ksiegarnia.Infrastructure.Extensions
{
    public static class MapperExtension
    {
        public static TDestination Map<TSource, TDestination>(
    this TDestination destination, TSource source)
        {
            return Mapper.Map(source, destination);
        }
    }
}
