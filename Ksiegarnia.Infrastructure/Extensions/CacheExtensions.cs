﻿using Ksiegarnia.Infrastructure.DTO;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ksiegarnia.Infrastructure.Extensions
{
   public static class CacheExtensions
    {
        public static void SetJwt(this IMemoryCache cache, Guid tokenId, JwtDto jwt)
            => cache.Set(GetJwtKey(tokenId), jwt, TimeSpan.FromSeconds(5));

        public static JwtDto GetJwt(this IMemoryCache cache, Guid tokenid)
            => cache.Get<JwtDto>(GetJwtKey(tokenid));

        public static string GetJwtKey(Guid tokenid)
            => $"jwt-{tokenid}";
    }
}
