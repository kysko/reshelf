﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ksiegarnia.Infrastructure.Extensions
{
    public static class StringExtension
    {
        public static bool Empty(this string value)
            => String.IsNullOrWhiteSpace(value);
            
        
    }
}
