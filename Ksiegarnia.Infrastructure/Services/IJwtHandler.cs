﻿//using Ksiegarnia.Infrastructure.DTO;
using Ksiegarnia.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ksiegarnia.Infrastructure.Services
{
    public interface IJwtHandler
    {
        JwtDto CreateToken(Guid userId, string role);
    }
}
