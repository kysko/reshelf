﻿using System;
using System.Collections.Generic;
using System.Text;
using Ksiegarnia.Core;
using Ksiegarnia.Core.Repositories;
using Ksiegarnia.Core.Domain;
using Ksiegarnia.Infrastructure.DTO;
using AutoMapper;
using System.Threading.Tasks;

namespace Ksiegarnia.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IEncrypter _encrypter;

        public UserService (IUserRepository userRepository,IEncrypter encrypter, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _encrypter = encrypter;
        }

        public async Task<UserDto> GetAsync(string email)
        {
            var user = await _userRepository.GetAsync(email);

            return _mapper.Map<User,UserDto>(user);
        }
        public async Task<IEnumerable<UserDto>>GetAllAsync()
        {
            var user = await _userRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<UserDto>>(user);



        }


        public async Task RegisterAsync(string email,string username,string City,string userSurname,string Telephone,string role, string password)
        {
            var user = await _userRepository.GetAsync(email);
            if (user != null)
            {
                throw new Exception($"User with email already'{email}' existss");
            }
            var salt = _encrypter.GetSalt(password);
            var hash = _encrypter.GetHash(password, salt);
            user = new User(email, hash,City, username,userSurname,Telephone, salt,User.UserRole.User.ToString());
            await _userRepository.AddAsync(user);
            
        }

        public async Task RemoveUserAsync(string email)
        {
            var user = await _userRepository.GetAsync(email);
           await _userRepository.RemoveAsync(user);
        }


        public async Task LoginAsync(string email, string password)
        {
            var user = await _userRepository.GetAsync(email);
            if (user == null)
            {
                throw new Exception(
                    "Invalid credentials");
            }

            var hash = _encrypter.GetHash(password, user.Salt);
            if (user.Password == hash)
            {
                return;
            }
            throw new Exception(
                "Invalid credentials");
        }

    }
}
