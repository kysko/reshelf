using Ksiegarnia.Core.Domain;
using Ksiegarnia.Infrastructure.DTO;
using System;
using System.Collections.Generic;

namespace Ksiegarnia.Infrastructure.Services
{
    public interface IBookUserService : IService
    {
        BookUserDto GetByBook(string Book);
        BookUserDto GetByUser(string User);
        IEnumerable<BookUserDto> GetAll();
        void Parse(Book book, User user);
    }
}
