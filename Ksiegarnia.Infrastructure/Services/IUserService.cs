﻿using Ksiegarnia.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ksiegarnia.Infrastructure.Services
{
   public interface IUserService : IService
    {
        Task<UserDto> GetAsync(string email);
        Task<IEnumerable<UserDto>> GetAllAsync();
        Task RegisterAsync(string email, string username, string City, string userSurname, string Telephone, string role, string password);
        Task RemoveUserAsync(string email);
        Task LoginAsync(string email, string password);

    }
}
