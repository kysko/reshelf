﻿using System;
using System.Collections.Generic;
using Ksiegarnia.Infrastructure.DTO;
using Ksiegarnia.Core.Repositories;
using AutoMapper;
using Ksiegarnia.Core.Domain;
using System.Threading.Tasks;
using Ksiegarnia.Infrastructure.Extensions;
namespace Ksiegarnia.Infrastructure.Services
{
    public class BookService : IBookService
    {
        private IBookRepository _bookRepository;
  

        private readonly IMapper _mapper;
        public BookService(IBookRepository bookRepository, IMapper mapper)
        {
           
            _bookRepository = bookRepository;
            _mapper = mapper;
        }
        public async Task <BookDto> GetAsync(string Title)
        {
            var book = await _bookRepository.GetAsync(Title);

            return _mapper.Map<Book, BookDto>(book);
        }
       
        


        public async Task<BookDto> GetAsync(Guid id)
        {
            var book =await  _bookRepository.GetAsync(id);
            return _mapper.Map<Book, BookDto>(book);
        }

        public async Task <IEnumerable<BookDto>> GetAllAsync()
        {
            var book = await _bookRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<BookDto>>(book);
        }
        public async Task<IEnumerable<SimpleBookDto>> GetAllSimpleAsync()
        {
            var book = await _bookRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<SimpleBookDto>>(book);
        }
        

        public async Task AddBook(string title, string author, string publisher, string publishDate, string language, string translator, string condition, string genre, Guid owner)
        {
            var book = new Book(title, author, publisher, publishDate, language, translator, condition, genre, owner);
            await _bookRepository.AddAsync(book);
        }

        public async Task<IEnumerable<BookUserDto>> GetBooksByUser(Guid userID)
        {
            var book = await _bookRepository.GetBooksByUser(userID);
  
            return _mapper.Map<IEnumerable<BookUserDto>>(book);
        }
    }
}
