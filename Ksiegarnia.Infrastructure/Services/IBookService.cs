﻿using Ksiegarnia.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ksiegarnia.Infrastructure.Services
{
    public interface IBookService :IService
    {
        Task<BookDto> GetAsync(string Title);
        Task<BookDto> GetAsync(Guid id);
        Task<IEnumerable<BookDto>> GetAllAsync();
        Task<IEnumerable<SimpleBookDto>> GetAllSimpleAsync();
        Task<IEnumerable<BookUserDto>> GetBooksByUser(Guid userID);
    
        Task AddBook(string title, string author, string publisher, string publishDate, string language,string translator,string condition,string genre,Guid owner);

    }
}
