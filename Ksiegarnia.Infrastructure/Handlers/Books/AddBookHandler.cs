﻿using Ksiegarnia.Infrastructure.Commands;
using Ksiegarnia.Infrastructure.Commands.Books;
using Ksiegarnia.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ksiegarnia.Infrastructure.Handlers.Books
{
    public class AddBookHandler : ICommandHandler<AddBook>
    {
        private readonly IBookService _bookService;
        
        public AddBookHandler(IBookService bookService)
        {
            _bookService = bookService;
        }

        public async Task HandleAsync(AddBook command)
        {
            await _bookService.AddBook(command.Title, command.Author, command.Publisher, command.PublishDate, command.Language, command.Translator, command.Condition, command.Genre, command.Owner);
        }
    }
}
