﻿using Ksiegarnia.Infrastructure.Commands;
using Ksiegarnia.Infrastructure.Commands.Users;
using Ksiegarnia.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ksiegarnia.Infrastructure.Handlers.Users
{
    public class CreateUserHandler : ICommandHandler<CreateUser>
    {
        private readonly IUserService _userService;
        public CreateUserHandler(IUserService userService)
        {
            _userService = userService;
       
        }
        public async Task HandleAsync(CreateUser command)
        {
            await _userService.RegisterAsync(command.Email, command.UserName,command.City,command.UserSurname,command.UserPhone,  command.Role, command.Password);
        }
    }
}
