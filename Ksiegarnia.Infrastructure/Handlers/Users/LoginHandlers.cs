﻿using Ksiegarnia.Infrastructure.Commands;
using Ksiegarnia.Infrastructure.Commands.Accounts;
using Ksiegarnia.Infrastructure.Extensions;
using Ksiegarnia.Infrastructure.Services;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ksiegarnia.Infrastructure.Handlers.Users
{
    public class LoginHandlers : ICommandHandler<Login>
    {
        private readonly IUserService _userService;
        private readonly IJwtHandler _jwtHandler;
        private readonly IMemoryCache _memorycache;


        public LoginHandlers(IUserService userService,IJwtHandler jwtHandler,
            IMemoryCache cache)
        {
            _userService = userService;
            _jwtHandler = jwtHandler;
            _memorycache = cache;
        }


        public async Task HandleAsync(Login command)
        {
            await _userService.LoginAsync(command.Email, command.Password);
            var user = await _userService.GetAsync(command.Email);
            var jwt = _jwtHandler.CreateToken(user.Id, user.Role);
            _memorycache.SetJwt(command.TokenId, jwt);
        }
    }
}
