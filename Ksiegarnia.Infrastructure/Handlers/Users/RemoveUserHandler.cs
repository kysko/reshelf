﻿using Ksiegarnia.Infrastructure.Commands;
using Ksiegarnia.Infrastructure.Commands.Users;
using Ksiegarnia.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ksiegarnia.Infrastructure.Handlers.Users
{
    public class RemoveUserHandler : ICommandHandler<RemoveUser>
    {
        private readonly IUserService _userService;
        public RemoveUserHandler(IUserService userService)
        {
            _userService = userService;

        }
   

        public async Task HandleAsync(RemoveUser command)
        {
            await _userService.RemoveUserAsync(command.Email);
        }
    }
}
