﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ksiegarnia.Infrastructure.Commands.Accounts
{
    public class Login : ICommand
    {
        public Guid TokenId { get; set; }
       // public Guid Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
