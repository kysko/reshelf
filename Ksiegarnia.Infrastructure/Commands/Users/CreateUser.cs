﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ksiegarnia.Infrastructure.Commands.Users
{
    public class CreateUser : ICommand
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string UserSurname { get; set; }
        public string UserPhone { get; set; }
        public string City { get; set; }
        public string Role { get; set; }
    }
}
