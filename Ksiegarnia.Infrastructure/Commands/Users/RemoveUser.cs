﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ksiegarnia.Infrastructure.Commands.Users
{
    public class RemoveUser : ICommand
    {

        public string Email { get; set; }
    }
}
