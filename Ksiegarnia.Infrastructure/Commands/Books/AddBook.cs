﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ksiegarnia.Infrastructure.Commands.Books
{
    public class AddBook : ICommand
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public string PublishDate { get;set; }
        public string PageNumber { get; set; }
        public string Language { get; set; }
        public string Translator { get;set; }
        public string Condition { get;set; }
        public string Genre { get;set; }
       // public string Cover { get;set; }
        public Guid Owner{ get; set; }


    }
}
