﻿using AutoMapper;
using Ksiegarnia.Core.Domain;
using Ksiegarnia.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ksiegarnia.Infrastructure.Mappers
{
    public static class AutoMapperConfig
    {
        public static IMapper Initialize() =>
            new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<Book, BookDto>();
                // cfg.CreateMap<Book, SimpleBookDto>();

                cfg.CreateMap<Book, BookUserDto>();
                cfg.CreateMap<User, BookUserDto>().ForMember(x => x.UserPhone,t => t.MapFrom(s => s.UserPhone)).ForMember(x=> x.Username,t => t.MapFrom(s => s.Username)).ForMember(x => x.UserSurname , t => t.MapFrom(s => s.UserSurname));

            }
            ).CreateMapper();
    }
}
