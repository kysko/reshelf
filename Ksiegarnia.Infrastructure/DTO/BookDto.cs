﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ksiegarnia.Infrastructure.DTO
{
    public class BookDto
    {
        //public Guid BookId { get; set; }

        public string Author { get; protected set; }
        public string Title { get; protected set; }
        public string PageNumber { get; protected set; }
        public string PublishDate { get; protected set; }
        public string Publish { get; protected set; }
        public string Language { get; protected set; }
        public string Translator { get; protected set; }
        public string Condition { get; protected set; }
        public string Genre { get; protected set; }

    }
}
