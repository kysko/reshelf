﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ksiegarnia.Infrastructure.DTO
{
    public class SimpleBookDto
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public Guid Id { get; set; }

    }
}
