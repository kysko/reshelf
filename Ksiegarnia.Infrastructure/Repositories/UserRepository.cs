﻿using Ksiegarnia.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Ksiegarnia.Core.Domain;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Ksiegarnia.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository , IMongoRepository
    {
        private readonly IMongoDatabase _database;
        private IMongoCollection<User> Users => _database.GetCollection<User>("Users");
       // private IMongoCollection<User> Users => _database.GetCollection<User>("Tests");
        public UserRepository(IMongoDatabase mongoDatabase)
        {
            _database = mongoDatabase;
        }

        public async Task AddAsync(User user)
        => await Users.InsertOneAsync(user);

        public async Task<IEnumerable<User>> GetAllAsync()
        => await Users.AsQueryable().ToListAsync();

        public async Task<User> GetAsync(Guid id)
        => await Users.AsQueryable().FirstOrDefaultAsync(x => x.Id == id);

        

        public async Task<User> GetAsync(string Email)
        => await Users.AsQueryable().FirstOrDefaultAsync(x => x.Email == Email);

        public async Task RemoveAsync(User email)
        => await Users.DeleteOneAsync(x=>x.Email == email.Email);

        public async Task UpdateAsync(User user)
        => await Users.ReplaceOneAsync(x => x.Id == user.Id, user);

       

    }
}
