﻿using Ksiegarnia.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Ksiegarnia.Core.Domain;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Ksiegarnia.Infrastructure.Repositories
{
    public class BookRepository : IBookRepository, IMongoRepository
    {

        private readonly IMongoDatabase _database;
        private IMongoCollection<Book> Books => _database.GetCollection<Book>("Books");

        public BookRepository(IMongoDatabase mongoDatabase)
        {
            _database = mongoDatabase;
        }

        public async Task AddAsync(Book book)
            => await Books.InsertOneAsync(book);

        public async Task<IEnumerable<Book>> GetAllAsync()
            => await Books.AsQueryable().ToListAsync();

        public async Task<Book> GetAsync(Guid id)
            => await Books.AsQueryable().FirstOrDefaultAsync(x => x.Id == id);

        public async Task<Book> GetAsync(string name)
            => await Books.AsQueryable().FirstOrDefaultAsync(x => x.Title == name);

        public async Task RemoveAsync(Guid id)
            => await Books.DeleteOneAsync(x => x.Id == id);

        public async Task UpdateAsync(Book book)
            => await Books.ReplaceOneAsync(x=> x.Id == book.Id, book);

        public async Task<IEnumerable<Book>> GetBooksByUser(Guid userID)
        => await Books.AsQueryable().Where(x => x.Owner == userID).ToListAsync();
    }
}
