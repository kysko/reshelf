﻿using Autofac;
using Microsoft.Extensions.Configuration;
using Ksiegarnia.Infrastructure.Settings;
using Ksiegarnia.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using Ksiegarnia.Infrastructure.MongoDB;

namespace Ksiegarnia.Infrastructure.IoC.Modules
{
    public class SettingsModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;
        public SettingsModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterInstance(_configuration.GetSettings<GeneralSettings>())
                .SingleInstance();
            builder.RegisterInstance(_configuration.GetSettings<MongoSettings>())
                .SingleInstance();
            builder.RegisterInstance(_configuration.GetSettings<JwtSettings>())
                .SingleInstance();
        }
    }
}
