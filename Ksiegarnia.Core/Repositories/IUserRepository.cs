﻿using System;
using System.Collections.Generic;
using System.Text;
using Ksiegarnia.Core.Domain;
using System.Threading.Tasks;

namespace Ksiegarnia.Core.Repositories
{
    public interface IUserRepository : IRepository
    {
        Task<User> GetAsync(Guid id);
        Task<User> GetAsync(string Email);
    
        Task<IEnumerable<User>> GetAllAsync();
        Task AddAsync(User user);
        Task UpdateAsync(User user);
        Task RemoveAsync(User email);
    }
}
