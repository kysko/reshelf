﻿using System;
using System.Collections.Generic;
using System.Text;
using Ksiegarnia.Core.Domain;
namespace Ksiegarnia.Core.Repositories
{
    public interface IBookUserRepository :IRepository
    {
        BookUser Get(Guid id);
        BookUser GetbyBook(string bookName);
        BookUser GetbyUser(string UserName);
        IEnumerable<BookUser> GetAll();
    }
}
