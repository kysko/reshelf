﻿using System;
using System.Collections.Generic;
using System.Text;
using Ksiegarnia.Core.Domain;
using System.Threading.Tasks;

namespace Ksiegarnia.Core.Repositories
{
   public interface IBookRepository : IRepository
    {   Task<Book> GetAsync(Guid id);
        Task<Book> GetAsync(string name);
        Task<IEnumerable<Book>> GetBooksByUser(Guid userID);
        Task<IEnumerable<Book>> GetAllAsync();
        Task AddAsync(Book book);
        Task UpdateAsync(Book book);
        Task RemoveAsync(Guid id);

    }
}
