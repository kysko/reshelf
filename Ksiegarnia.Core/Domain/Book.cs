﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Ksiegarnia.Core.Domain
{
   public class Book
    {
        private static readonly Regex NameRegex = new Regex("^(?![_.-])(?!.*[_.-]{2})[a-zA-Z0-9._.-]+(?<![_.-])$");
        private static readonly Regex NumberRegex = new Regex("[0-9]$");

      
    
       
        public Guid Id { get; protected set; }
        public Guid Owner { get; protected set; }
        public string Author { get; protected set; }
        public string Title { get; protected set; }
        public string PageNumber { get; protected set; }
        public string PublishDate { get; protected set; }
        public string Publish { get; protected set; }
        public string Language { get; protected set; }
        public string Translator { get; protected set; }
        public string Condition { get; protected set; }
        public string Genre { get; protected set; }
        public string Cover { get; protected set; }
        public int Count { get; protected set; }
        public string Price { get; protected set; }
        public DateTime UpdatedAt { get; protected set; }
        public DateTime CreatedAt { get; protected set; }

        protected Book()
        {
        }

        public Book(string title, string author, string publisher, string publishDate, string language, string translator, string condition, string genre, Guid owner)
        {
            Id = Guid.NewGuid();
            SetTitle(title);
            SetAutor(author);
            Publish = publisher;
            PublishDate = publishDate;
            Language = language;
            Translator = translator;
            Condition = condition;
            Genre = genre;
            Owner = owner;
          //  Price = price ?? throw new ArgumentNullException(nameof(price));
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }
        private void SetAutor(string autor)
        {
            if (string.IsNullOrWhiteSpace(autor) || !NameRegex.IsMatch(autor) )
            {
               Author="Brak Danych";
            }
        
            Author = autor;
        }

        private void SetTitle(string title)
        {
           /* if (string.IsNullOrWhiteSpace(title) || !NameRegex.IsMatch(title) )
            {
               Author="Brak Danych";
            }*/
            Title = title;
        }
        private void SetPageNumber(string page)
        {
            if(NumberRegex.IsMatch(page)){
             PageNumber = page;
            }
            PageNumber= "Brak Danych";

        }
        private void SetPublishDate(string date){
            
            if(string.IsNullOrWhiteSpace(date)){
                PublishDate= "Brak Danych";
            }
            PublishDate=date;
        }
        private void Update(){
            UpdatedAt = DateTime.UtcNow;
        }


    }
}
