﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ksiegarnia.Core.Domain
{
    public class BookUser
    {
        public Guid BookId { get; protected set; }
        public Book Book { get; protected set; }
        public User User { get; protected set; }
        public IEnumerable<Book> UserBooks { get; protected set; }
    }
}
