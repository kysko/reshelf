﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Ksiegarnia.Core.Domain
{
    public class User
    {
        public enum UserRole
        {
            User,
            Admin
        };
        private static readonly Regex NameRegex = new Regex("^(?![_.-])(?!.*[_.-]{2})[a-zA-Z0-9._.-]+(?<![_.-])$");

        public Guid Id { get; protected set; }
        public string Email { get; protected set; }
        public string Password { get; protected set; }
        public string Salt { get; protected set; }
        public string Username { get; protected set; }
        public string UserSurname { get; protected set; }
        public string UserPhone { get; set; }
        public string City { get; set; }
        public string Role { get; protected set; }
        public IEnumerable<BookUser> BookUser {get;protected set;}
        public DateTime CreatedAt { get; protected set; }
        public DateTime UpdatedAt { get; protected set; }

        protected User()
        {
        }

        public User(string email, string password, string City, string username, string userSurname, string Telephone, string salt,  string role)
        {
            Id = Guid.NewGuid();
            SetEmail(email);
            SetPassword(password, salt);
            SetUsername(username);
            City = this.City;
            Username = username;
            Telephone = UserPhone;
            SetRole(role);
            CreatedAt = DateTime.UtcNow;
        }

        private void SetRole(string role)
        {
            if (string.IsNullOrWhiteSpace(role)){
                //TODO EXCEPTIONS
                role = UserRole.User.ToString();
            }
            if (Role == role)
            {
                return;
            }
            Role = role;
            UpdatedAt = DateTime.UtcNow;
        }

        private void SetUsername(string username)
        {

            Username = username.ToLower();
            UpdatedAt = DateTime.UtcNow;
        }

        private void SetEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                //TODO EXCEPTIONS
                throw new Exception();
            }
            if (Email == email)
            {
                return;
            }

            Email = email.ToLowerInvariant();
            UpdatedAt = DateTime.UtcNow;
        }



        private void SetPassword(string password, string salt)
        {
            if (string.IsNullOrWhiteSpace(password)){
                //TODO EXCEPTIONS
                throw new Exception();
            }
            if (string.IsNullOrWhiteSpace(salt))
            {
                //TODO EXCEPTIONS
                throw new Exception();
            }
            if(password.Length<4)
            {
                //TODO EXCEPTIONS
                throw new Exception();
            }
            if (password.Length > 100)
            {
                //TODO EXCEPTIONS
                throw new Exception();
            }
            if (password == Password)
            {
                //TODO EXCEPTIONS
                throw new Exception();
            }
            Password = password;
            Salt = salt;
            UpdatedAt = DateTime.UtcNow;
        }
    }
}
