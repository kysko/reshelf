﻿using Ksiegarnia.Infrastructure.Commands;
using Ksiegarnia.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ksiegarnia.Api.Controllers
{
    public class AccountController : ApiControllerBase
    {
        
        private readonly IJwtHandler _jwtHandler;
        private readonly IUserService _userService;
        public AccountController(ICommandDispatcher commandDispatcher,IJwtHandler jwtHandler, IUserService userService)
            : base(commandDispatcher)
        {
            _userService = userService;
            _jwtHandler = jwtHandler;
        }
        /*
        [HttpGet("{email}")]
        public async Task<IActionResult> Get(string email)
        {
            
            var user = await _userService.GetAsync(email);
            var token = _jwtHandler.CreateToken(user.Id, "user");
            return Json(token);
        }
        */

        /*
        [HttpPut]
        [Route("password")]
        public async Task<IActionResult> Put([FromBody]ChangeUserPassword command)
        {
            await DispatchAsync(command);

            return NoContent();
        }*/
    }
}
