using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ksiegarnia.Infrastructure.Services;
using Ksiegarnia.Infrastructure.DTO;
using Ksiegarnia.Infrastructure.Commands;
using Microsoft.AspNetCore.Authorization;
using Ksiegarnia.Infrastructure.Commands.Books;
using Ksiegarnia.Infrastructure.Settings;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Ksiegarnia.Api.Controllers
{
    [Route("[controller]")]
    public class BookController : ApiControllerBase
    {

        private readonly IBookService _bookService;
        public BookController(IBookService bookService,ICommandDispatcher commandDispatcher, GeneralSettings settings) :base (commandDispatcher)
        {
            _bookService = bookService;
        }


       // [Authorize]
        [HttpGet]
        public async Task< IEnumerable<BookDto>> GetAsync()
        => await _bookService.GetAllAsync();
        
        [HttpGet("{Title}")]
        public async Task<BookDto> GetAsync(string Title)
            => await _bookService.GetAsync(Title);
        /*
        [HttpGet("id")]
        public async Task<BookDto> GetAsync(Guid id)
            => await _bookService.GetAsync(id);
       */
        [HttpGet("/books")]
        public async Task<IEnumerable<SimpleBookDto>> GetSimpleAsync()
       => await _bookService.GetAllSimpleAsync();

        [Authorize]
        [HttpPost("/add")]
        public async Task<IActionResult> AddAsync([FromBody]AddBook command)
        {



            var owner = new Guid(User.Identity.Name);
      
            command.Owner=owner ;
            await _commandDispatcher.DispatchAsync(command);
            return Created($"books/{command.Author}", new object());//return succ
        }
        //dodac req na Username/*
        /*
        [HttpGet("{username}")]
        public UserDto GetUser(string username)
        {
            return _bookService.Get(username);
        }*/
    }
}
