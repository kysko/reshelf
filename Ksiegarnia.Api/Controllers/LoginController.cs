﻿using Ksiegarnia.Infrastructure.Commands;
using Ksiegarnia.Infrastructure.Commands.Accounts;
using Ksiegarnia.Infrastructure.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Ksiegarnia.Api.Controllers
{
    public class LoginController :ApiControllerBase
    {
        private readonly IMemoryCache _memoryCache;

        public LoginController(ICommandDispatcher commandDispatcher, IMemoryCache memoryCache)
            :base(commandDispatcher)
        {
            _memoryCache = memoryCache;
        }

        public async Task<IActionResult>Post([FromBody]Login command)
        {
            command.TokenId = Guid.NewGuid();
            await _commandDispatcher.DispatchAsync(command);
            var jwt = _memoryCache.GetJwt(command.TokenId);
            if (jwt.Token.Empty())
            {
                
                return StatusCode(406);
            }
            return Json(jwt);

        }
    }
}
