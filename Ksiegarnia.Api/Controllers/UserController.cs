﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ksiegarnia.Infrastructure.Services;
using Ksiegarnia.Infrastructure.DTO;
using Ksiegarnia.Infrastructure.Commands;
using Ksiegarnia.Infrastructure.Commands.Users;
using Ksiegarnia.Infrastructure.Settings;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Ksiegarnia.Api.Controllers
{   //[Authorize]
    [Route("[controller]")]
    public class UserController : ApiControllerBase
    {

        private readonly IUserService _userService;
        private readonly IBookService _bookService;
        public UserController(IUserService userService,IBookService bookService ,ICommandDispatcher commandDispatcher, GeneralSettings settings):base(commandDispatcher)
        {
            _userService = userService;
            _bookService = bookService;
        }
        //[Authorize]
        [HttpGet]
        public async Task <IEnumerable<UserDto>> Get()
        => await _userService.GetAllAsync();

        //[Authorize]
        [HttpGet("/userbooks/{email}")]
        public async Task<IActionResult> Get(string email)
        {
            var user= await _userService.GetAsync(email);
            var book = await _bookService.GetBooksByUser(user.Id);
            return Json(book);
        }
        [AllowAnonymous]
        [HttpPost("/register")]

        public async Task<IActionResult> Post([FromBody]CreateUser command)
        {
            await _commandDispatcher.DispatchAsync(command);
            return Created($"users/{command.Email}", new object());
        }
        //[Authorize]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]RemoveUser command)
        {
            await _commandDispatcher.DispatchAsync(command);
            return Created($"users/{ command.Email}",new object());
        }
        
    }
}
