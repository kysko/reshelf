# Book: 
	
	 Get
 	/book/ :
		return IEnumerable list of books on server with full data
    /book/{Title}:
		search for given book title in DB retun NULL if not found
	/book/{ID}:
		search for given book ID in DB retun NULL if not found
	/book/books:
		return simplified list of books on server (title,author,owner)
	
 	Add
	/book/add (Post)
		from body:         
		  string Title  
          string Author  
          string Publisher  
          string PublishDate  
          string PageNumber  
          string Language  
          string Translator  
          string Condition  
       	  string Genre  
          string Cover  
         
	*not documented:
	    Task<BookDto> GetAsync(string Title);
        Task<BookDto> GetAsync(Guid id);
        Task<IEnumerable<BookDto>> GetAllAsync();
        Task<IEnumerable<SimpleBookDto>> GetAllSimpleAsync();
		Task<BookDto> GetClose(string Cityname,float distance);<-return books from city/nearby
		

# Login:
	/login (Post)
	    string Email 
        string Password 
	return JWT tokken
# User:
	/user/register (Post)
		from body:
		  string Email  
          string Password  
          string UserName  
          string UserSurname  
          string UserPhone  
          string City  
          string Role (if authorized as Admin can be set to Admin otherwise default(User)) 
	
   	Remove user:
	
	/user/{me}   (Delete)
	delete current logged user
	*if authorized as admin /user/{email} remove user with given mail*
	
	/user/{email}
		return information about given user (posted books etc)
	
	*not documented:
	    Task<UserDto> GetAsync(string email);
        Task<IEnumerable<UserDto>> GetAllAsync();
        Task RegisterAsync(string email, string username,string role, string password);
        Task RemoveUserAsync(string email);
		Task CheckMSG();
	    BookUserDto GetByBook(string Book);
        BookUserDto GetByUser(string User);
        IEnumerable<BookUserDto> GetAll();
        void Parse(Book book, User user);
# Other :
	